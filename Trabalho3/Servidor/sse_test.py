from datetime import datetime
from flask import Flask, send_from_directory, render_template, Response
from flask_sse import sse
from apscheduler.schedulers.background import BackgroundScheduler

import os
import time

app = Flask(__name__)
app.config["REDIS_URL"] = "redis://localhost"
app.register_blueprint(sse, url_prefix='/stream')

@app.route('/')
def index():
    return('200 - ok :D')

@app.route('/stream')
def stream():
    def eventStream():
        while True:
            time.sleep(5)
            yield "id: 0\nevent: sc\ndata: held\n\n"
    return Response(eventStream(), mimetype="text/event-stream")

@app.route('/sse-stream')
def ssestream():
    while True:
        time.sleep(5)
        sse.publish({"message": datetime.now()}, type='publish')
    return 'ok'
        

if __name__ == '__main__':
    app.run()