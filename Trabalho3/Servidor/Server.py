#!/usr/bin/env python
# encoding: utf-8
import json
import sys
import time
import os
import datetime
from http.server import BaseHTTPRequestHandler, HTTPServer
from threading import Timer
from flask import Flask, request, jsonify
from flask_restful import Resource, Api
from flask_cors import CORS, cross_origin
from flask_sse import sse


# waitress-serve --port=5000 Aplicativo:app

app = Flask(__name__)
app.config["REDIS_URL"] = "redis://localhost"
app.register_blueprint(sse, url_prefix='/stream')
app.app_context().push()
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

recurso1 = ''
recurso2 = ''
fila_acesso1 = []
fila_acesso2 = []
tempo_fila1 = 5
tempo_fila2 = 10
timer_fila1 = None
timer_fila2 = None
global_client_name = ''

def printStatus():
    global recurso1,recurso2,fila_acesso1,fila_acesso2,tempo_fila1,tempo_fila2,timer_fila1,timer_fila2
    os.system('cls' if os.name == 'nt' else 'clear')
    print("Recurso 1: " + str(recurso1))
    print("Recurso 2: " + str(recurso2))
    print("Fila de acesso recurso 1: ", fila_acesso1)
    print("Fila de acesso recurso 2: ", fila_acesso2)

##Basic timer - executa uma função a cada intervalo "tempo" de tempo
def timer1(tempo,client_name):  
    global recurso1,recurso2,fila_acesso1,fila_acesso2,tempo_fila1,tempo_fila2,timer_fila1,timer_fila2,global_client_name
    print('timer iniciado: ' + time.strftime('%H:%M:%S'))  #releaseToken,[(client)]
    timer_fila1=Timer(tempo,releaseSC_timer,[(client_name)])  # espera 10 segundos antes de chamar a função display
    timer_fila1.start() # Here run is called  

def timer2(tempo,client_name):  
    global recurso1,recurso2,fila_acesso1,fila_acesso2,tempo_fila1,tempo_fila2,timer_fila1,timer_fila2,global_client_name
    print('timer iniciado: ' + time.strftime('%H:%M:%S'))  #releaseToken,[(client)]
    timer_fila2=Timer(tempo,releaseSC_timer,[(client_name)])  # espera 10 segundos antes de chamar a função display
    timer_fila2.start() # Here run is called  

def notificar_cliente(client_name,mensagem):
    sse.publish({"message": mensagem, "data": None}, type=client_name)
    print('mandou sse')

def releaseSC_timer(client_name):
    with app.app_context():
        global recurso1,recurso2,fila_acesso1,fila_acesso2,tempo_fila1,tempo_fila2,timer_fila1,timer_fila2
        if(recurso1 == client_name):
            timer_fila1.cancel()
            notificar_cliente(client_name,'RELEASED')
            if(len(fila_acesso1)>0):
                recurso1 = fila_acesso1.pop(0)
                notificar_cliente(recurso1,'HELD')
                timer1(tempo_fila1,recurso1)
            else:
                recurso1 = ''
        elif(recurso2 == client_name):
            timer_fila2.cancel()
            notificar_cliente(client_name,'RELEASED')
            if(len(fila_acesso2)>0):
                recurso2 = fila_acesso2.pop(0)
                notificar_cliente(recurso2,'HELD')
                timer2(tempo_fila2, recurso2)
            else:
                recurso2 = ''
        printStatus()

    return


@app.route('/release', methods=['POST'])
def releaseSC():
    global recurso1,recurso2,fila_acesso1,fila_acesso2,tempo_fila1,tempo_fila2,timer_fila1,timer_fila2,global_client_name
    client_name = request.json['name']
    if(recurso1 == client_name):
        timer_fila1.cancel()
        notificar_cliente(client_name,'RELEASED')
        if(len(fila_acesso1)>0):
           recurso1 = fila_acesso1.pop(0)
           notificar_cliente(recurso1,'HELD')
           timer1(tempo_fila1,recurso1)
        else:
            recurso1 = ''
    elif(recurso2 == client_name):
        timer_fila2.cancel()
        notificar_cliente(client_name,'RELEASED')
        if(len(fila_acesso2)>0):
            recurso2 = fila_acesso2.pop(0)
            notificar_cliente(recurso2,'HELD')
            timer2(tempo_fila2, recurso2)
        else:
            recurso2 = ''
    printStatus()
    return 'ok'

@app.route('/request', methods=['POST'])
def requestSC():
    global recurso1,recurso2,fila_acesso1,fila_acesso2,tempo_fila1,tempo_fila2,timer_fila1,timer_fila2
    message = ''
    # pegando os dados da requisição
    client_name = request.json['name']
    recurso = request.json['recurso']
    operatoin = request.json['operation']
    # ocupando seção crítica
    if(operatoin == 'request'):
        if(recurso == '1'):
            if(recurso1 == ''):
                recurso1 = client_name
                timer1(tempo_fila1,client_name)
                notificar_cliente(client_name,'HELD')
                message = 'HELD'
            else:
                fila_acesso1.append(client_name)
                message = 'WANTED'
        else:
            if(recurso2 == ''):
                recurso2 = client_name
                timer2(tempo_fila2,client_name)
                notificar_cliente(client_name,'HELD')
                message = 'HELD'
            else:
                fila_acesso2.append(client_name)
                message = 'WANTED'
    else:
        releaseSC(client_name)
        message = 'RELEASED'
    printStatus()
    return message


# main task
if __name__ == '__main__':
    app.run()