const axios = require('axios').default;


class Client {
    constructor(state, name, recurso) {
      this.state = state;
      this.name = name;
      this.recurso = recurso;
      const EventSource = require('eventsource');
      this.fonte = new EventSource('http://127.0.0.1:5000/stream');
      this.ouvir_servidor(name);
    }

    print_status() {
      // limpando o terminal
      console.clear();
      // imprimindo as informações do cliente
      this.state = this.fonte.estado
      if(this.state == 'RELEASED')
      {
          this.recurso = ''
      }
      console.log(this.name.toString());
      console.log('State: ' + this.state.toString());
      console.log('Recurso: ' + this.recurso.toString());
      //console.log('Fonte: ' + this.fonte.estado)
    }

    async request_recurso(recurso) {
        // changing client status
        this.recurso = recurso;
        this.fonte.estado = 'WANTED';
        console.log('Enviando requisicao')
        // SIM: requisita entrada na sessão crítica
        await axios.post('http://localhost:5000/request', {
            recurso: this.recurso.toString(),
            operation: 'request',
            name: this.name.toString()
            })
            // .then(function (response) {
            //     const data = response['data'];
            //     state = data.toString();
            // })
            .catch(function (error) {
                console.log(error);
            });
        this.print_status();
        return 0;
    }
    

    ouvir_servidor(nome_usuario){
        console.log('ouvindo servidor');
        this.fonte.estado = 'RELEASED'
        this.fonte.addEventListener(nome_usuario, function(event){
            let dados = event.data
            let dados_json = JSON.parse(dados)
            console.log("\n-------Nova mensagem do servidor: " + dados_json['message'] + "\n")
            this.estado = dados_json['message']
        });
    }
    

    async release_recurso() {
        // changing client status
        this.recurso = '';
        this.state = 'RELEASED';
        await axios.post('http://localhost:5000/release', {
            recurso: this.recurso.toString(),
            operation: 'release',
            name: this.name.toString()
            })
            // .then(function (response) {
            //     let dados = response['data'];
            // })
            .catch(function (error) {
                console.log(error);
            });
        this.print_status();
        return 0;
    }
    
  }

// rodando a main
main()

async function main() {
    // criando o cliente
    var client = new Client('RELEASED', new Date().getSeconds(), '');
    client.print_status();
    // loop da main
    while(true) {
        // Pegando a opção do usuário
        const readline = require('readline-sync');
        let recurso = readline.question("Insira o recurso a ser requisitado ou liberado: ");
        if(recurso == 'x'){
            client.print_status()
        }else
        {
            if(client.state == "RELEASED") {
                await client.request_recurso(recurso);
            } else {
                await client.release_recurso();
            }      
        }

        await new Promise(r => setTimeout(r, 200));
        //client.print_status()
    }
}


