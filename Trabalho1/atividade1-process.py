from queue import Queue
import time
import os
import threading

#states:
RELEASED = 'RELEASED'
HELD = 'HELD'
WANTED = 'WANTED'


#ip do grupo multicast
ip_group_multicast = 'multicast' #qualquer um do grupo pode ver

#message queue
msgQ = Queue()

# CRIANDO O PROCESSO -------------------------------------------------------------------------
class Process (threading.Thread):
    def __init__(self,name):      
        threading.Thread.__init__(self)
        #Name of process
        self.name = name

        #init state
        self.state = RELEASED    

        #unicasts reqeusts/answers received
        self.responses = []

        #mensagens recebidas enquanto o processo estava na sessão crítica ou enquanto o processo estava esperando por mais tempo
        self.criticalSectionQueue = []
        return
    
    def join_group(self,multicast_ip):
        self.multicast_ip = multicast_ip #simularia um socket na vida real

        return


    def listen_unicast(self):
        #se a fila de mensagens não está vazia e se as mensagens são para ele
        if not msgQ.empty() and (msgQ.queue[0])[0] == self.name:
            self.responses.append(msgQ.get()[1])
            
        return
    
    def listen_multicast(self):
        #verifica se tem mensagens no canal de comunicação (Queue) 
        #quer ver qual é a primeira mensagem da fila, se ela pertence ao grupo de multicast -> só da pra ver uma mensagem do grupo se você está nele
        #vericiando se a mensagem não foi enviada pela própria thread
        if not msgQ.empty() and (msgQ.queue[0])[0] == self.multicast_ip and (msgQ.queue[0])[1][1]!= self.name:
            message = msgQ.get()[1] #retira a mensagem da queue, pega a msg de verdade, a hora e o nome, cabeçalho cai fora
            if(self.state == HELD or (self.state == WANTED and message[0]>self.time)): #não quer responder, manda pra caixa postal
                self.criticalSectionQueue.append(message[1])
            else:
                self.send_unicast(message[1]) #ta livre pra responder e não tem interesse na SC
        
        return

    def send_unicast(self, destination): #envia uma mensagem para a fila de respostas
        #send message to specific process
        message = (destination,(time.time(),self.name)) #manda cabeçalho de destino
        msgQ.put(message) #bota na fila de mensagem e o processo vai saber que é para ele
        
        #send message to process

    def send_multicast(self): #botar minha requisição no canal compartilhado - envia mensagem para a fila de espera (Queue)
        msg_time = time.time()
        message = (self.multicast_ip,(msg_time, self.name)) #envelopado num header de UDP simulado, envolto no destino = endereço do multicast
        msgQ.put(message) #gambiarra -> como o listen tira a mensagem da Queue, colocamos na Queue um número de mensagens igual ao número de processos
        msgQ.put(message)
        return msg_time

    def enterSection(self):
        if(self.state == RELEASED):
            self.state = WANTED
            self.time = self.send_multicast() #recebe o tempo que enviou a mensagem solicitando a entrada na SC
        return  


    def exitSection(self):
        if self.state == HELD:
            self.state = RELEASED
            for destination in self.criticalSectionQueue:
                self.send_unicast(destination)

            self.criticalSectionQueue.clear()
        return

    def run(self):
        while(True):
            #no caso de sockets os processos usariam uma lógica de eventos e seriam notificados quando houvessem novas mensagens
            #as threads ouvindo sempre multicast e unicast simulam isso
            self.listen_multicast()
            self.listen_unicast()
            #pode entrar na sc
            if(self.state == WANTED) and len(self.responses) >= 2:
                self.state = HELD
                self.responses.clear()
            #como só temos um processador o sleep muda a prioridade do processador para passar ele para a próxima thread (incluindo a main)
            time.sleep(0.1)
        

# MAIN -------------------------------------------------------------------------
        
def main():
    #creating the proccesses
    a = Process('a')
    b = Process('b')
    c = Process('c')

    #processos entrando no grupo multicast
    a.join_group(ip_group_multicast) #qualquer processo do grupo pode ver mensagens com o cabeçado de multicast
    b.join_group(ip_group_multicast)
    c.join_group(ip_group_multicast)

    #para as threads encerrarem quando a thread main encerrar
    a.daemon = True
    b.daemon = True
    c.daemon = True

    #o start das thread chama a função run
    a.start()
    b.start()
    c.start()

    while(True):
        #cleaning screen
        os.system('CLS')

        #printing menu
        print('\nPROCESSO |    ESTADO    |        FILA DE REQUISIÇÔES        |        FILA DE RESPOSTAS        |')
        print('-----------------------------------------------')
        print('    ' + a.name + '    |   ' + a.state + '   |   ' + str(a.criticalSectionQueue) + '   |   ' + str(a.responses))
        print('    ' + b.name + '    |   ' + b.state + '   |   ' + str(b.criticalSectionQueue) + '   |   ' + str(b.responses))
        print('    ' + c.name + '    |   ' + c.state + '   |   ' + str(c.criticalSectionQueue) + '   |   ' + str(c.responses))
     
        print('\nPor favor, escolha uma ação: ')
        print('  1. Requisitar a seção crítica')
        print('  2. Liberar a seção crítica')
        print('  3. Sair')
        print('  4. Atualizar interface')
        user_option = input('  > ') #para a main thread e vai para as outras, por isso não precisa de sleep, mas por isso precisa atualizar

        #menu options
        if(user_option == '1'):
            print('\nSelecione o processo que irá requisitar a SC:')
            user_option = input(' > ')
            if(user_option == 'a'):
                a.enterSection()
            elif(user_option == 'b'):
                b.enterSection()
            elif(user_option == 'c'):
                c.enterSection()
            else:
                print('Opção inválida :(')
        elif(user_option == '2'):
            print('\nSelecione o processo que irá sair da SC:')
            user_option = input(' > ')
            if(user_option == 'a'):
                a.exitSection()
            elif(user_option == 'b'):
                b.exitSection()
            elif(user_option == 'c'):
                c.exitSection()
            else:
                print('Opção inválida :(')
        elif(user_option == '3'):
            break



if __name__ == "__main__":
    main()
