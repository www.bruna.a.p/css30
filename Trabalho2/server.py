import sys
import Pyro4
import time  
from threading import Timer  
import os
from cryptography.hazmat.primitives.asymmetric.ed25519 import Ed25519PrivateKey
from cryptography.hazmat.primitives import serialization

# configura uma instância única do servidor para ser consumida por diversos  clientes 
@Pyro4.behavior(instance_mode="single") 
class servidor(object): 
    def __init__(self):
        self.recurso1 =''
        self.recurso2 =''
        self.fila_acesso1 = []
        self.fila_acesso2 = []
        self.tempo_fila1 = 5
        self.tempo_fila2 = 10
        self.clients = []
        self.private_key = Ed25519PrivateKey.generate()
        self.public_key = self.private_key.public_key()
        self.signature = self.private_key.sign(b"here is server")
        

    ##Basic timer - executa uma função a cada intervalo "tempo" de tempo
    def timer1(self,tempo,clientName):  
        print('timer iniciado: ' + time.strftime('%H:%M:%S'))  #self.releaseToken,[(client)]
        self.timer_fila1=Timer(tempo,self.releaseToken,[(clientName)])  # espera 10 segundos antes de chamar a função display
        self.timer_fila1.start() # Here run is called  

    def timer2(self,tempo,clientName):  
        print('timer iniciado: ' + time.strftime('%H:%M:%S'))  #self.releaseToken,[(client)]
        self.timer_fila2=Timer(tempo,self.releaseToken,[(clientName)])  # espera 10 segundos antes de chamar a função display
        self.timer_fila2.start() # Here run is called  

    @Pyro4.expose
    def printStatus(self,msg):
        os.system('CLS')
        print(msg)
        if self.recurso1 == '':
            print("Recurso 1: ")
        else:
            print("Recurso 1: Cliente " + str(self.recurso1) )
        
        if self.recurso2 == '':
            print("Recurso 2: ")
        else:
            print("Recurso 2: Cliente " + str(self.recurso2))
            
        print("Fila de acesso recurso 1: ", self.fila_acesso1)
        print("Fila de acesso recurso 2: ", self.fila_acesso2)

    @Pyro4.expose
    @Pyro4.oneway
    def requestToken(self, recurso, clientName):
        if(recurso == '1'):
            if self.recurso1 == '':
                self.recurso1 = clientName
                print('timer iniciado: ' + time.strftime('%H:%M:%S')) 
                self.clients[clientName].notificarAcesso(1,self.signature)
                self.timer1(self.tempo_fila1,clientName)  
            else:
                self.fila_acesso1.append(clientName)
        else:
            if self.recurso2 == '':
                self.recurso2 = clientName
                print('timer iniciado: ' + time.strftime('%H:%M:%S')) 
                self.clients[clientName].notificarAcesso(2,self.signature)
                self.timer2(self.tempo_fila2,clientName)  
            else:
                self.fila_acesso2.append(clientName)
        
        self.printStatus(("Cliente "+ str(clientName) + " requisitou acesso ao recurso " + str(recurso)))

    @Pyro4.expose
    def addClient(self, callback):
        client = callback
        self.clients.append(client)
        self.printStatus('')
        public_bytes = self.public_key.public_bytes(encoding=serialization.Encoding.Raw, 
        format=serialization.PublicFormat.Raw)
        return (len(self.clients)-1,public_bytes)
        


    @Pyro4.expose
    @Pyro4.oneway
    def releaseToken(self, clientName):
        if clientName == self.recurso1:
            self.timer_fila1.cancel()
            self.clients[clientName].notificarLiberacao(self.signature)
            msg = ("Recurso 1 liberado")
            if len(self.fila_acesso1):
                self.recurso1 = self.fila_acesso1.pop(0)
                self.clients[self.recurso1].notificarAcesso(1,self.signature)
                self.timer1(self.tempo_fila1,self.recurso1)  
            else:
                self.recurso1 =''
        elif clientName == self.recurso2:
            self.timer_fila2.cancel()
            self.clients[clientName].notificarLiberacao(self.signature)
            msg = ("Recurso 2 liberado")
            if len(self.fila_acesso2):
                self.recurso2 = self.fila_acesso2.pop(0)
                self.clients[self.recurso2].notificarAcesso(2,self.signature)
                self.timer2(self.tempo_fila2,self.recurso2)  
            else:
                self.recurso2 =''
        print('Tempo finalizado: ' + time.strftime('%H:%M:%S'))          
        self.printStatus(msg)


# registra a aplicação do servidor no serviço de nomes 
daemon = Pyro4.Daemon() 
ns = Pyro4.locateNS()  
uri = daemon.register(servidor) 
ns.register("NomeAplicacaoServidor", uri)  
daemon.requestLoop() 
print("A aplicação está ativa")
