from email.mime import base
from unicodedata import name
import Pyro4 
import threading
import time
import os
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import ed25519
import base64

class Client:
    def __init__(self,server):
        self.name = ''
        self.state = 'RELEASED'
        self.recurso = ''
        self.servidor = server
    

    def printStatus(self):
        os.system('CLS')
        print("Cliente: " + str(self.name))
        print("State: " + str(self.state))
        print("Recurso: " + str(self.recurso))


    def notify(self):
        print("callback recebido do servidor!") 

    def notifyAccess(self,recurso):
        self.recurso = str(recurso)
        self.state = 'HELD'
        client.printStatus()
        

    def notifyRelease(self):
        print("Recurso " + str(self.recurso) +" liberado ")
        self.recurso = ''
        self.state = 'RELEASED'
        client.printStatus()

    def clientRequestToken(self,recurso):
        self.recurso = recurso
        self.state = 'WANTED'
        self.servidor.requestToken(recurso, self.name)
        client.printStatus()

    def clientReleaseToken(self):
        self.servidor.releaseToken(self.name)
    



class cliente_callback(object): 
    abort = 0
    def __init__(self, client):
        self.client = client

    @Pyro4.expose 
    @Pyro4.callback 
    def setName(self,name):
        self.client.name = name

    @Pyro4.expose 
    @Pyro4.callback 
    def notificacao(self,signature): 
        self.publicKey.verify(base64.b64decode(signature.get('data')),b"here is server")
        self.client.notify()

    @Pyro4.expose 
    @Pyro4.callback 
    def notificarAcesso(self, recurso,signature):
        self.publicKey.verify(base64.b64decode(signature.get('data')),b"here is server")
        self.client.notifyAccess(recurso)
        
    @Pyro4.expose 
    @Pyro4.callback 
    def notificarLiberacao(self,signature):
        self.publicKey.verify(base64.b64decode(signature.get('data')),b"here is server")
        self.client.notifyRelease()

    
    def addKey(self,key):
        self.publicKey = ed25519.Ed25519PublicKey.from_public_bytes(base64.b64decode(key.get('data')))

    @Pyro4.expose 
    @Pyro4.callback 
    def loopThread(self, daemon): 
        # thread para ficar escutando chamadas de método do servidor  
        daemon.requestLoop(lambda: not self.abort) 

# obtém a referência da aplicação do servidor no serviço de nomes  
ns = Pyro4.locateNS() 
uri = ns.lookup("NomeAplicacaoServidor") 
servidor = Pyro4.Proxy(uri) 
# … servidor.metodo() —> invoca método no servidor 
#inicialia cliente
client = Client(servidor)

# Inicializa o Pyro daemon e registra o objeto Pyro callback nele.  
daemon = Pyro4.core.Daemon() 
callback = cliente_callback(client) #—> callback será enviado ao servidor   
daemon.register(callback) 

dictionary = servidor.addClient(callback)
client.name =  dictionary[0]
key = dictionary[1]

callback.addKey(key)


#inicializa a thread para receber notificações do servidor 
thread = threading.Thread(target=callback.loopThread, args=(daemon, ))  
thread.daemon =True 
thread.start() 
client.printStatus()
print("Recurso a ser requisitado ou requisitar liberação: ")
while True:
    #client.printStatus()
    recurso = input()
    if(client.state == 'RELEASED'):
        client.clientRequestToken(recurso)
    elif(client.state == 'HELD'):
        client.clientReleaseToken()
    

